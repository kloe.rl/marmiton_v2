import { useState } from 'react'
import heartIcon_notLiked from "../assets/heart_off.png"
import heartIcon_isLiked from "../assets/heart_on.png"

export default function Catalogue({ url, width, height, name }) {

    const [isFavorite, setFavorite] = useState(false)

    return (
        <>
            <section className="catalogue__container">
                <div className="catalogue__imgContainer">
                    <img className="catalogue__img" src={url} alt="" />
                </div>
                <div className="catalogue__specs">
                    <input
                        onClick={() => setFavorite(!isFavorite)}
                        className="catalogue__heartIcon" type="image"
                        src={isFavorite ? heartIcon_isLiked : heartIcon_notLiked} />
                    <h2 className="catalogue__catName">{name}</h2>
                    {/* <p className="catalogue__size">{"Size : " + width + " x " + height}</p> */}
                </div>
            </section>
        </>
    )
};