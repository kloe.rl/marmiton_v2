import axios from "axios";
import { useState, useEffect } from "react"

import './App.css'
import './components/Cat.css'
import './components/Catalogue.css'

import Cat from './components/Cat'
import Catalogue from './components/Catalogue'
// import CatalogueData from "./components/CatalogueData";

import { dataCatList } from './dataCatList.js'
import { dataCatName } from './dataCatName.js'
import Header from "./components/Header";

function App() {
  const theCatApi = "https://api.thecatapi.com/v1/images/search?limit=10"
  const [catalogue, setCatalogue] = useState([]);

  useEffect(() => {
    axios.get(theCatApi).then((response) => {

      const tempCatalogue = response.data
      const newArrayCat = tempCatalogue.map((eachCat, i) => {
         const idName = dataCatName[i]
         eachCat.name = idName.name
         return eachCat
      })
      setCatalogue(newArrayCat);
      // console.log(newArrayCat)
    });
  }, [])

  return (
    <>
      <section className="header">
        <Header />
      </section>
      <section className="cat__container">
        {dataCatList.map((item) => (
          <Cat {...item} />
        ))}
      </section>
      <section className="cat__catalogue">
        <h3 className="cat__catalogueDescription">Meet new cats around your area !</h3>
        {catalogue.map((eachCat, i) => (
          <Catalogue
            key={eachCat.id}
            {...eachCat}
          />
        ))}
      </section>
    </>
  )
}

export default App


