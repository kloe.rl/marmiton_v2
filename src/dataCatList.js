const dataCatList = [
    {
      name: "Quinoa",
      url:"url(https://images.unsplash.com/photo-1514888286974-6c03e2ca1dba?q=80&w=2043&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)",
      mood:"Studious & Career-wise"
    },
    {
      name: "Célestin",
      url:"url(https://images.unsplash.com/photo-1581300134629-4c3a06a31948?q=80&w=1887&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)",
      mood:"Always on time"
    },
    {
      name: "Border-radius",
      url:"url(https://images.unsplash.com/photo-1561948955-570b270e7c36?q=80&w=1801&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)",
      mood:"Loves puns about fish"
    },
    {
      name: "Vévé",
      url:"url(https://images.unsplash.com/photo-1579168765467-3b235f938439?q=80&w=1888&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)",
      mood:"Human's comfyness expert"
    },
    {
      name: "Sergeï",
      url: "url(https://images.unsplash.com/photo-1536589961747-e239b2abbec2?q=80&w=2995&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)",
      mood:"Defies the laws of gravity"
    }
  ];

  export { dataCatList };