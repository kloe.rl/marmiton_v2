import React from 'react'
import './header.css'

// Main logo
import logo from '../assets/wool-ball.png'
// Menu icon
import menu from '../assets/list.png'
// Nav Icons
import mag from '../assets/bookmark.png'
import ideas from '../assets/shopping-bag.png'
import impress from '../assets/microphone.png'
import news from '../assets/email.png'
import art from '../assets/settings.png'
import award from '../assets/star.png'

function Header() {

    return (
        <>
            <section className="header__container">
                <section className="header-top">
                    <img src={menu} alt="" className="menu-burger icon" />
                    <div className="header__divider"></div>
                    <div className="name-logo">
                        <img src={logo} alt="" className="logo icon" />
                        <h1 className="site__name">Catify</h1>
                    </div>
                    <input type="text" className="header__input" placeholder="I'm looking for..."/>
                    <div className="header__divider"></div>
                    <button className="header__btn-login">Connexion</button>
                </section>
                <section className="header-bottom">
                    <nav className="header__navContainer">
                        <ul className="header__navUl">
                            <div className="nav__mag">
                                <img src={mag} alt="" className="mag-icon nav__icon icon" />
                                <li>The Mag</li>
                            </div>
                            <div className="nav__ideas">
                                <img src={ideas} alt="" className="mag-icon nav__icon icon" />
                                <li>Human's treats for Christmas</li>
                            </div>
                            <div className="nav__impress">
                                <img src={impress} alt="" className="mag-icon nav__icon icon" />
                                <li>Impress your human</li>
                            </div>
                            <div className="nav__news">
                                <img src={news} alt="" className="mag-icon nav__icon icon" />
                                <li>Cat's News</li>
                            </div>
                            <div className="nav__artg">
                                <img src={art} alt="" className="mag-icon nav__icon icon" />
                                <li>Mastering Cat Art</li>
                            </div>
                            <div className="nav__award">
                                <img src={award} alt="" className="mag-icon nav__icon icon" />
                                <li>Meowz Award 2023</li>
                            </div>
                        </ul>
                    </nav>
                </section>
            </section>
        </>
    )
}

export default Header