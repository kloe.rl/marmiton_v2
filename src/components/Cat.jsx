import { useState } from 'react'
import heartIcon_notLiked from "../assets/heart_off.png"
import heartIcon_isLiked from "../assets/heart_on.png"

export default function Cat({url, name, mood}) {

    const [isLiked, setIsLiked] = useState(false);

    return (
        <section className="cat__card" style={{ backgroundImage: url }}>
            <input className="heartIcon" onClick={() => setIsLiked(isLiked ? false : true)}
                type="image" src={isLiked ? heartIcon_isLiked : heartIcon_notLiked} alt="heart icon" 
            />
            <div className="cat-info__card">
                <h2 className="cat__name">{name}</h2>
                <p className="cat__mood">{mood}</p>
            </div>
        </section>
    )
}